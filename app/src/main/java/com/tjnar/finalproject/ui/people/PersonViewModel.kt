package com.tjnar.finalproject.ui.people

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.tjnar.finalproject.data.Repository
import com.tjnar.finalproject.data.models.Person


class PersonViewModel(app: Application) : AndroidViewModel(app) {
    private val appRepository = Repository.getInstance(app)

    // People
    val people = appRepository.people
    val selectedPerson = MutableLiveData<Person>()

    fun addPerson(person: Person) = appRepository.addPerson(person)

    fun updatePerson(person: Person) = appRepository.updatePerson(person) {
        appRepository.refreshPeople()
    }
}
