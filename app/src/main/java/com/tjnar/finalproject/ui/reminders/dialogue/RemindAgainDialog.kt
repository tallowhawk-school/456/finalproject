package com.tjnar.finalproject.ui.reminders.dialogue

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.tjnar.finalproject.R

class RemindAgainDialog(private val yes: DialogInterface.OnClickListener,
                        private val no: DialogInterface.OnClickListener): DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {
            AlertDialog.Builder(it)
                .setMessage(R.string.set_up_new_time)
                .setPositiveButton(R.string.yes, yes)
                .setNegativeButton(R.string.no, no)
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireDialog().setCanceledOnTouchOutside(false)
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}