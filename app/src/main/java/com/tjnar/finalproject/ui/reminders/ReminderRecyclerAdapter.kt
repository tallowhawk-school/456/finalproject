package com.tjnar.finalproject.ui.reminders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.data.relationships.ReminderRelationship
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ReminderRecyclerAdapter(
    private val reminderRelationships: List<ReminderRelationship>,
    private val recyclerActions: RecyclerActions
) :
    RecyclerView.Adapter<ReminderRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.reminder_recycler, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = reminderRelationships.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reminder = reminderRelationships[position]
        with(holder) {
            personNameText.text = reminder.person.name
            reminderStatus.text = reminder.reminderType.name
            dateText.text = reminder.reminder.time.toString()
            recyclerLayout.setOnClickListener {
                recyclerActions.onRowClick(reminder)
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val personNameText: TextView = itemView.findViewById(R.id.person_name_text)
        val reminderStatus: TextView = itemView.findViewById(R.id.reminder_status)
        val dateText: TextView = itemView.findViewById(R.id.date_text)
        val recyclerLayout: RelativeLayout = itemView.findViewById(R.id.reminder_recycler_container)
    }

    fun deleteItem(position: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            recyclerActions.deleteItem(position)
            CoroutineScope(Dispatchers.Main).launch {
                notifyItemRemoved(position)
            }
        }
    }

    interface RecyclerActions {
        fun deleteItem(position: Int)

        fun onRowClick(reminder: ReminderRelationship)
    }
}
