package com.tjnar.finalproject.ui.people

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.tjnar.finalproject.util.PERMISSIONS_REQUEST_READ_CONTACTS
import com.tjnar.finalproject.util.PICK_CONTACT
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.models.Person
import com.tjnar.finalproject.util.contacts.getEmail
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class PeopleFragment : Fragment(), PeopleRecyclerAdapter.PersonRowListener {
    private val DETAIL_QUERY_ID = 50

    private lateinit var personViewModel: PersonViewModel
    private lateinit var navController: NavController
    private lateinit var recyclerView: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }

        setHasOptionsMenu(true)
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        personViewModel =
            ViewModelProviders.of(requireActivity()).get(PersonViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_people, container, false)

        recyclerView = root.findViewById(R.id.peopleRecyclerView)

        personViewModel.people.observe(this, Observer {
            val adapter = PeopleRecyclerAdapter(requireContext(), it, this)
            recyclerView.adapter = adapter
        })
        return root
    }

    override fun onPersonRowClick(person: Person) {
        personViewModel.selectedPerson.value = person
        navController.navigate(R.id.profileFragment)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_addPerson -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(
                    Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
               requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS),
                   PERMISSIONS_REQUEST_READ_CONTACTS
               )
            } else {
                val intent = Intent(Intent.ACTION_PICK)
                    .apply {
                        type = ContactsContract.Contacts.CONTENT_TYPE
                    }
                startActivityForResult(intent, PICK_CONTACT)
            }
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            val intent = Intent(Intent.ACTION_PICK)
                .apply {
                    type = ContactsContract.Contacts.CONTENT_TYPE
                }
            startActivityForResult(intent, PICK_CONTACT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            PICK_CONTACT -> {
                if (resultCode == Activity.RESULT_OK) {
                    val contactData = data?.data
                    if (contactData != null) {
                        getBasicContactInfo(contactData)
                    }
                }
            }
        }
    }

    private fun getBasicContactInfo(contactData: Uri) {
        CoroutineScope(Dispatchers.IO).launch {
            val person = Person()
            activity?.contentResolver?.query(contactData, null, null, null, null)
                .use { cursor ->
                    if (cursor != null && cursor.moveToFirst()) {

                        cursor.getColumnIndex(ContactsContract.Data.LOOKUP_KEY).also {
                            person.contactId = cursor.getString(it)
                        }

                        cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME).also {
                            person.name = cursor.getString(it)
                        }
                    }
                }
            val phoneNumber = getPhoneNumber(person.contactId)
            val email = getEmail(person.contactId, activity)
            person.phoneNumber = phoneNumber
            person.email = email
            personViewModel.addPerson(person)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.app_bar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    suspend fun getPhoneNumber(contactId: String): String {
        val selection = "${ContactsContract.Data.LOOKUP_KEY} = ?"
        val selectionArgs: Array<String> = arrayOf(contactId)
        var phoneNumber = ""

        activity?.contentResolver?.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            selection,
            selectionArgs,
            null
        )
            .use { cursor ->
                if (cursor != null && cursor.moveToFirst()) {
                    phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                }

            }
        return phoneNumber
    }
}