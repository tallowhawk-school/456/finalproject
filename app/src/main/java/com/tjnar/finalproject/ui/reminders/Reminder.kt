package com.tjnar.finalproject.ui.reminders


import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.tjnar.finalproject.R
import com.tjnar.finalproject.ui.reminders.dialogue.RemindAgainDialog
import com.tjnar.finalproject.ui.people.PersonViewModel
import kotlinx.android.synthetic.main.fragment_reminder.view.*

/**
 * A simple [Fragment] subclass.
 */
class Reminder : Fragment() {

    lateinit var remindersViewModel: RemindersViewModel
    lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_reminder, container, false)

        navController = Navigation.findNavController(
            requireActivity(), R.id.nav_host_fragment
        )

        remindersViewModel =
            ViewModelProviders.of(requireActivity()).get(RemindersViewModel::class.java)

        remindersViewModel.selectedReminder.observe(this, Observer {
            root.person_name_text.text = it.person.name
            root.reminder_status.text = it.reminderType.name
            root.date_text.text = it.reminder.time.toString()

            if (it.reminderType.id == 2L) {
                root.schedule_checkbox.isChecked = true
                root.schedule_to_meet_button.isEnabled = false
            }
        })

        root.schedule_to_meet_button.setOnClickListener {
            remindersViewModel.newReminderState = RemindersViewModel.SCHEDULE
            navController.navigate(R.id.newReminderFragment)
        }

        root.mark_complete_button.setOnClickListener {
            root.meetup_checkbox.isChecked = true
            RemindAgainDialog(yes = yesListener, no = noListener)
                .show(requireActivity().supportFragmentManager, "RemindAgain")
        }

        root.view_profile_button.setOnClickListener {
            with(ViewModelProviders.of(requireActivity()).get(PersonViewModel::class.java)) {
                remindersViewModel.selectedReminder.value?.let {
                    selectedPerson.value = it.person
                }
            }
            navController.navigate(R.id.profileFragment)
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            navController.navigateUp()
        }
        return super.onOptionsItemSelected(item)
    }

    private val yesListener = DialogInterface.OnClickListener { _, _ ->
        remindersViewModel.selectedReminder.value?.let {
            remindersViewModel.removeReminder(it.reminder)
        }

        remindersViewModel.newReminderState = RemindersViewModel.RESCHEDULE
        navController.navigate(R.id.newReminderFragment)
    }

    private val noListener = DialogInterface.OnClickListener { _, _ ->
        remindersViewModel.selectedReminder.value?.let {
            remindersViewModel.removeReminder(it.reminder)
        }
        navController.popBackStack()
    }
}
