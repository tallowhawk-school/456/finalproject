package com.tjnar.finalproject.ui.reminders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.relationships.ReminderRelationship

class Reminders : Fragment(), ReminderRecyclerAdapter.RecyclerActions {

    private lateinit var remindersViewModel: RemindersViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        remindersViewModel =
            ViewModelProviders.of(requireActivity()).get(RemindersViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_reminders, container, false)

        recyclerView = root.findViewById(R.id.reminder_recycler_view)

        remindersViewModel.reminders.observe(this, Observer {

            val adapter = ReminderRecyclerAdapter(it.sortedBy { reminderRelationship ->
                reminderRelationship.reminder.time
            }, this)
            recyclerView.adapter = adapter
            val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(adapter, context!!))
            itemTouchHelper.attachToRecyclerView(recyclerView)
        })
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.app_bar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_addPerson -> {
            remindersViewModel.newReminderState = RemindersViewModel.INITIAL
            navController.navigate(R.id.newReminderFragment)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onRowClick(reminder: ReminderRelationship) {
        remindersViewModel.selectedReminder.value = reminder
        navController.navigate(R.id.reminder)
    }

    override fun deleteItem(position: Int) {
        remindersViewModel.reminders.value?.get(position)?.let {
            remindersViewModel.removeReminder(it.reminder)
        }
    }
}
