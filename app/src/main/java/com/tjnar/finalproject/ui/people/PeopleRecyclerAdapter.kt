package com.tjnar.finalproject.ui.people

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.models.Person
import com.tjnar.finalproject.util.contacts.getPhotoUri
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PeopleRecyclerAdapter(val context: Context,
                            val people: List<Person>,
                            val itemListener: PersonRowListener) :
        RecyclerView.Adapter<PeopleRecyclerAdapter.ViewHolder>() {

    override fun getItemCount(): Int = people.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.people_recycler, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val person = people[position]
        with(holder) {
            personText.apply {
                text = person.name
                contentDescription = person.name
            }

            updatePhoto(profilePhoto, person.contactId)

            phoneNumber.apply {
                text = person.phoneNumber
                contentDescription = person.phoneNumber
            }

            email.apply {
                text = person.email
                contentDescription = person.email
            }

            body.text = person.notes

            holder.itemView.setOnClickListener {
                itemListener.onPersonRowClick(person)
            }
        }
    }

    private fun updatePhoto(image: ImageView, id: String) {

        CoroutineScope(Dispatchers.IO).launch {
            val photoURI = getPhotoUri(id, context)

            CoroutineScope(Dispatchers.Main).launch {
                if (photoURI != null) {
                    image.setImageURI(photoURI)
                } else {
                    image.setImageResource(R.drawable.ic_person_black_24dp)
                }
            }
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val personText: TextView = itemView.findViewById(R.id.personNameText)
        val profilePhoto: ImageView = itemView.findViewById(R.id.profilePicture)
        val phoneNumber: TextView = itemView.findViewById(R.id.tvPhoneNumber)
        val email: TextView = itemView.findViewById(R.id.tvEmail)
        val body: TextView = itemView.findViewById(R.id.tvLongBody)
    }

    interface PersonRowListener {
        fun onPersonRowClick(person: Person)
    }
}
