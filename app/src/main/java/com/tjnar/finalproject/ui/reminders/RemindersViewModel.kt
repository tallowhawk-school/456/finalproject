package com.tjnar.finalproject.ui.reminders

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.tjnar.finalproject.data.Repository
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.data.relationships.ReminderRelationship

class RemindersViewModel(app: Application) : AndroidViewModel(app) {
    private val appRepository = Repository.getInstance(app)

    val people = appRepository.people

    val reminders = appRepository.reminders

    val selectedReminder = MutableLiveData<ReminderRelationship>()

    var newReminderState = INITIAL

    fun addReminder(reminder: Reminder, then: (() -> Unit)? = null) = appRepository.addReminder(reminder) {
        appRepository.refreshReminders {
            then?.invoke()
        }
    }

    fun updateReminder(reminder: Reminder, then: (() -> Unit)? = null) {
        appRepository.updateReminder(reminder) {
            appRepository.refreshReminders {
                then?.invoke()
            }
        }
    }

    fun removeReminder(reminder: Reminder) {
        appRepository.deleteReminder(reminder) {
            appRepository.refreshReminders()
        }
    }

    companion object REMINDER_STATE {
        const val INITIAL = "initial_reminder_state"
        const val SCHEDULE = "schedule_reminder_state"
        const val RESCHEDULE = "reschedule_reminder_state"
    }
}
