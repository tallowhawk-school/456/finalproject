package com.tjnar.finalproject.ui.profile


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.tjnar.finalproject.R
import com.tjnar.finalproject.util.contacts.getPhotoUri
import com.tjnar.finalproject.ui.people.PersonViewModel
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {
    private lateinit var personViewModel: PersonViewModel
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        navController = Navigation.findNavController(
            requireActivity(), R.id.nav_host_fragment
        )

        personViewModel =
            ViewModelProviders.of(requireActivity()).get(PersonViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profile, container, false)

        personViewModel.selectedPerson.observe(this, Observer {
            root.nameTextView.text = it.name
            root.bioEditText.setText(it.notes)
            updateProfileImage(it.contactId)
            root.phone_number_text_view.text = it.phoneNumber
            root.email_text_view.text = it.email
        })

        root.phoneImageView.setOnClickListener {
            val phoneNumber = personViewModel.selectedPerson.value?.phoneNumber
            if (phoneNumber != null && phoneNumber.isNotBlank()) {
                dialPhoneNumber(phoneNumber)
            }
        }

        root.mailImageView.setOnClickListener {
            val email = personViewModel.selectedPerson.value?.email
            if (email != null && email.isNotBlank()) {
                composeEmail(arrayOf(email), "")
            }
        }

        root.save_body_button.setOnClickListener {
            it?.let { v ->
                val imm =
                    requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(v.windowToken, 0)
            }
            personViewModel.selectedPerson.value?.let {
                personViewModel.updatePerson(it.copy(notes = bioEditText.text.toString()))
            }
            bioEditText.clearFocus()
            Toast.makeText(context, R.string.toast_saved, Toast.LENGTH_LONG).show()
        }

        // Inflate the layout for this fragment
        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            navController.navigateUp()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateProfileImage(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val photoURI = getPhotoUri(id, activity, false)
            CoroutineScope(Dispatchers.Main).launch {
                if (photoURI != null) {
                    profileImageView.setImageURI(photoURI)
                } else {
                    profileImageView.setImageResource(R.drawable.ic_person_black_24dp)
                }
            }
        }
    }

    private fun dialPhoneNumber(phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:$phoneNumber")
        }
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun composeEmail(addresses: Array<String>, subject: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:") // only email apps should handle this
            putExtra(Intent.EXTRA_EMAIL, addresses)
            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(intent)
        }
    }
}
