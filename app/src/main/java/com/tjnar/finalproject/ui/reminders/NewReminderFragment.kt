package com.tjnar.finalproject.ui.reminders


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.models.Person
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.util.locale
import com.tjnar.finalproject.util.tdns
import kotlinx.android.synthetic.main.fragment_new_reminder.*
import kotlinx.android.synthetic.main.fragment_new_reminder.view.*
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

/**
 * A simple [Fragment] subclass.
 */
class NewReminderFragment : Fragment() {

    private lateinit var remindersViewModel: RemindersViewModel
    private lateinit var navController: NavController

    private var currentDate: Date? = null
    private var selectedPerson: Person? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        val root = inflater.inflate(R.layout.fragment_new_reminder, container, false)
        val spinner = root.findViewById<Spinner>(R.id.person_drop_down)

        remindersViewModel =
            ViewModelProviders.of(requireActivity()).get(RemindersViewModel::class.java)

        val simpleNameArray: List<String>?

        if (remindersViewModel.newReminderState === RemindersViewModel.SCHEDULE) {
            root.title_text.text = getString(R.string.meetup_title)
            val name = remindersViewModel.selectedReminder.value?.person?.name
            simpleNameArray = listOf(name.toString())

            selectedPerson = remindersViewModel.selectedReminder.value!!.person
        } else if (remindersViewModel.newReminderState === RemindersViewModel.RESCHEDULE) {
            root.title_text.text = getString(R.string.meetup_title)
            val name = remindersViewModel.selectedReminder.value?.person?.name
            simpleNameArray = listOf(name.toString())

            selectedPerson = remindersViewModel.selectedReminder.value!!.person
        } else {
            simpleNameArray = remindersViewModel.people.value?.map { person -> person.name }

            val people = remindersViewModel.people
            if (people.value != null && people.value!!.isNotEmpty()) {
                selectedPerson = remindersViewModel.people.value?.get(0)
            }
        }


        if (simpleNameArray != null) {
            ArrayAdapter<String>(
                requireActivity(),
                R.layout.support_simple_spinner_dropdown_item,
                simpleNameArray
            )
                .also {
                    it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                    spinner.adapter = it
                }
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                remindersViewModel.people.value?.get(position).also { selectedPerson = it }
            }

        }

        root.set_time_date_button.setOnClickListener(handleGetDateTime)

        root.finish_button.setOnClickListener {
            if (currentDate != null) {
                if (selectedPerson != null) {
                    if (remindersViewModel.newReminderState == RemindersViewModel.SCHEDULE) {
                        remindersViewModel.selectedReminder.value?.reminder?.let { reminder ->
                            reminder.copy(
                                reminderTypeId = 2,
                                // force unwrap is okay here because we never set currentDate back to null
                                time = currentDate!!,
                                notificationSent = false
                            ).also {
                                remindersViewModel.updateReminder(it)
                            }
                        }
                    } else {
                        Reminder(0, currentDate!!, false, 1, selectedPerson!!.id).also {
                            remindersViewModel.addReminder(it)
                        }
                    }

                }
                navController.navigate(R.id.navigation_reminders)
            } else {
                Toast.makeText(context, R.string.no_date_selected_text, Toast.LENGTH_LONG).show()
            }
        }

        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            navController.navigateUp()
        }
        return super.onOptionsItemSelected(item)
    }

    private val handleGetDateTime = View.OnClickListener {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val currentLocal = locale(resources)

        val inputFormat = SimpleDateFormat("yyyy MM dd HH:mm", currentLocal)
        val outputFormat = SimpleDateFormat("EEEE, MMM d yyyy h:mm aa", currentLocal)

        DatePickerDialog(
            requireActivity(),
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                month.toString()
                val mHour = c.get(Calendar.HOUR_OF_DAY)
                val mMinute = c.get(Calendar.MINUTE)
                TimePickerDialog(
                    requireActivity(),
                    TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                        val pickedCalendar = Calendar.getInstance()
                        pickedCalendar.time =
                            inputFormat.parse(
                                "$year ${tdns(month + 1)} ${tdns(dayOfMonth)} ${tdns(hourOfDay)}:${tdns(
                                    minute
                                )}"
                            )!!

                        date_time_text_view.text = outputFormat.format(pickedCalendar.time)
                        currentDate = pickedCalendar.time
                    }, mHour, mMinute, false
                ).show()
            }, mYear, mMonth, mDay
        ).show()
    }
}
