package com.tjnar.finalproject.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tjnar.finalproject.data.converters.TimestampDateConverter
import com.tjnar.finalproject.data.dao.PersonDAO
import com.tjnar.finalproject.data.dao.ReminderDAO
import com.tjnar.finalproject.data.dao.ReminderTypeDAO
import com.tjnar.finalproject.data.models.Person
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.data.models.ReminderType

@Database(entities = [Person::class, Reminder::class, ReminderType::class], version = 1, exportSchema = false)
@TypeConverters(TimestampDateConverter::class)
abstract class ConnectionDatabase : RoomDatabase() {

    abstract fun personDAO(): PersonDAO
    abstract fun reminderDAO(): ReminderDAO
    abstract fun reminderTypeDAO(): ReminderTypeDAO

    companion object {
        @Volatile
        private var INSTANCE: ConnectionDatabase? = null

        fun getDatabase(context: Context): ConnectionDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        ConnectionDatabase::class.java,
                        "connections.db"
                    ).build()
                }
            }
            return INSTANCE!!
        }
    }
}
