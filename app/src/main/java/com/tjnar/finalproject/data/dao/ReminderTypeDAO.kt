package com.tjnar.finalproject.data.dao

import androidx.room.*
import com.tjnar.finalproject.data.models.ReminderType

@Dao
interface ReminderTypeDAO {
    @Query("SELECT * FROM reminder_types")
    fun getAll(): List<ReminderType>

    @Insert
    suspend fun insertReminderType(reminderType: ReminderType)

    @Insert
    suspend fun insertReminderTypes(reminderTypes: List<ReminderType>)

    @Update
    suspend fun updateReminderType(reminderType: ReminderType)

    @Delete
    suspend fun deleteReminderType(reminderType: ReminderType)
}
