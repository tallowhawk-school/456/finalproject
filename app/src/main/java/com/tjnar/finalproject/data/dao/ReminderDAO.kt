package com.tjnar.finalproject.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.data.relationships.ReminderRelationship

@Dao
interface ReminderDAO {
    @Query("SELECT * FROM reminders")
    fun getAllSync(): List<ReminderRelationship>

    @Query("SELECT * FROM reminders")
    suspend fun getAll(): List<ReminderRelationship>

    @Query("SELECT * FROM reminders WHERE id = :reminderId LIMIT 1")
    suspend fun getById(reminderId: Long): ReminderRelationship?

    @Insert
    suspend fun insertReminder(reminder: Reminder)

    @Insert
    suspend fun insertReminders(reminders: List<Reminder>)

    @Update
    suspend fun updateReminder(reminder: Reminder)

    @Delete
    suspend fun deleteReminder(reminder: Reminder)
}