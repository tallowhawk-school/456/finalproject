package com.tjnar.finalproject.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "people")
data class Person(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var name: String = "",
    var frequency: Int = 0,
    var notes: String = "",
    var contactId: String = "-1",
    var phoneNumber: String = "",
    var email: String = ""
)
