package com.tjnar.finalproject.data.relationships

import androidx.room.Embedded
import androidx.room.Relation
import com.tjnar.finalproject.data.models.Person
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.data.models.ReminderType

data class ReminderRelationship(
    @Embedded
    val reminder: Reminder,

    @Relation(parentColumn = "person_id", entityColumn = "id")
    val person: Person,

    @Relation(parentColumn = "reminder_type_id", entityColumn = "id")
    val reminderType: ReminderType
)
