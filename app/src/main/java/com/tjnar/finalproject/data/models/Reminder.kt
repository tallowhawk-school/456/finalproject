package com.tjnar.finalproject.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "reminders",
        foreignKeys = [
            ForeignKey(
                entity = ReminderType::class,
                parentColumns = ["id"],
                childColumns = ["reminder_type_id"]
            ),
            ForeignKey(
                entity = Person::class,
                parentColumns = ["id"],
                childColumns = ["person_id"]
            )
        ]
)
data class Reminder(
    @PrimaryKey(autoGenerate = true)
    val id: Long,

    val time: Date,

    @ColumnInfo(name = "notification_sent")
    val notificationSent: Boolean = false,

    @ColumnInfo(name = "reminder_type_id")
    val reminderTypeId: Long,

    @ColumnInfo(name = "person_id")
    val personId: Long
)
