package com.tjnar.finalproject.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reminder_types")
data class ReminderType(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val name: String
)
