package com.tjnar.finalproject.data.dao

import androidx.room.*
import com.tjnar.finalproject.data.models.Person

@Dao
interface PersonDAO {

    @Query("SELECT * FROM people")
    suspend fun getAll(): List<Person>

    @Query("SELECT * FROM people WHERE contactId = :contactId")
    fun getByContactId(contactId: String): List<Person>

    @Query("SELECT * FROM PEOPLE WHERE id = :id LIMIT 1")
    fun get(id: Long): Person

    @Insert
    suspend fun insertPerson(person: Person)

    @Insert
    suspend fun insertPeople(people: List<Person>)

    @Update
    suspend fun updatePerson(person: Person)

    @Delete
    suspend fun deletePerson(person: Person)
}
