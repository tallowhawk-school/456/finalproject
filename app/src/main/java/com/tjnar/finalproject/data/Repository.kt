package com.tjnar.finalproject.data

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.models.Person
import com.tjnar.finalproject.data.models.Reminder
import com.tjnar.finalproject.data.relationships.ReminderRelationship
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class Repository(val app: Application) {
    companion object {
        @Volatile
        private var INSTANCE: Repository? = null

        fun getInstance(app: Application): Repository {
            if (INSTANCE == null) {
                INSTANCE = Repository(app)
            }
            return INSTANCE!!
        }
    }


    private val personDAO = ConnectionDatabase.getDatabase(app).personDAO()
    private val reminderDAO = ConnectionDatabase.getDatabase(app).reminderDAO()

    val people = MutableLiveData<List<Person>>()
    val reminders = MutableLiveData<List<ReminderRelationship>>()

    init {
        CoroutineScope(Dispatchers.IO).launch {
            people.postValue(personDAO.getAll())
            reminders.postValue(reminderDAO.getAll())
        }
    }

    fun refreshPeople() {
        CoroutineScope(Dispatchers.IO).launch {
            people.postValue(personDAO.getAll())
        }
    }

    fun addPerson(person: Person) {
        CoroutineScope(Dispatchers.IO).launch {
            val people: List<Person> = personDAO.getByContactId(person.contactId)
            if (people.isNotEmpty()) {
                val dbPerson: Person = people[0]
                dbPerson.apply {
                    name = person.name
                    frequency = person.frequency
                    notes = person.notes
                }

                personDAO.updatePerson(dbPerson)

                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(app, R.string.person_already_exists, Toast.LENGTH_LONG).show()
                }
            } else {
                personDAO.insertPerson(person)
                refreshPeople()
            }
        }
    }

    fun updatePerson(person: Person, then: (() -> Unit)?) {
        CoroutineScope(Dispatchers.IO).launch {
            personDAO.updatePerson(person)
            then?.invoke()
        }
    }

    fun getRemindersSync(): List<ReminderRelationship> = reminderDAO.getAllSync()

    suspend fun getReminderById(reminderId: Long): ReminderRelationship? {
        return reminderDAO.getById(reminderId)
    }

    fun addReminder(reminder: Reminder, then: (() -> Unit)? = null) {
        CoroutineScope(Dispatchers.IO).launch {
            reminderDAO.insertReminder(reminder)
            then?.invoke()
        }
    }

    fun updateReminder(reminder: Reminder, then: (() -> Unit)? = null) {
        CoroutineScope(Dispatchers.IO).launch {
            reminderDAO.updateReminder(reminder)
            then?.invoke()
        }
    }

    fun deleteReminder(reminder: Reminder, then: (() -> Unit)? = null) {
        CoroutineScope(Dispatchers.IO).launch {
            reminderDAO.deleteReminder(reminder)
            then?.invoke()
        }
    }

    fun refreshReminders(then: (() -> Unit)? = null) {
        CoroutineScope(Dispatchers.IO).launch {
            reminders.postValue(reminderDAO.getAll())
            then?.invoke()
        }
    }
}
