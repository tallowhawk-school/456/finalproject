package com.tjnar.finalproject.data.converters

import androidx.room.TypeConverter
import java.util.*

class TimestampDateConverter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimeStamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}
