package com.tjnar.finalproject.util.broadcast

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import com.tjnar.finalproject.util.LOG_TAG
import com.tjnar.finalproject.util.boot.RefreshAlarmsService

class OnBootBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent != null && intent.action == "android.intent.action.BOOT_COMPLETED") {
            Log.d(LOG_TAG, "On boot receiver")

            if (context != null) {
                RefreshAlarmsService.enqueueWork(context, intent)
            }

        }

    }

    companion object {
        fun enableBootReceiver(context: Context) {
            val receiver = ComponentName(context, OnBootBroadcastReceiver::class.java)

            context.packageManager.setComponentEnabledSetting(
                receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP
            )
        }

    }
}
