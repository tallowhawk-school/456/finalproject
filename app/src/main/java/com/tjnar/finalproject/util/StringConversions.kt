package com.tjnar.finalproject.util

fun tdns(number: Int) = String.format("%02d", number)
fun tdns(number: Double) = String.format("%02.0f", number)
