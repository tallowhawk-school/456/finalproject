package com.tjnar.finalproject.util.notifications

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.JobIntentService
import com.tjnar.finalproject.R
import com.tjnar.finalproject.data.Repository
import com.tjnar.finalproject.data.relationships.ReminderRelationship
import com.tjnar.finalproject.util.LOG_TAG
import com.tjnar.finalproject.util.NOTIFICATION_INTENT_ID
import java.util.Date

class NotificationService : JobIntentService() {
    override fun onCreate() {
        super.onCreate()
        Log.d(LOG_TAG, "Notification Service onCreate")
    }

    override fun onHandleWork(intent: Intent) {
        val repository = Repository.getInstance(application)

        Log.d(LOG_TAG, "Notification Service Handle Intent")

        val now = Date()

        repository.getRemindersSync().forEach { reminderObj ->
            val reminder = reminderObj.reminder
            if (!reminder.notificationSent && reminder.time.before(now)) {
                notify(reminderObj)
                repository.updateReminder(reminder.copy(notificationSent = true))
            }
        }
    }

    private fun notify(reminder: ReminderRelationship) {
            val notificationText: String = if (reminder.reminderType.id == 1L) {
                application.getString(R.string.schedule_reminder_text)
            } else {
                application.getString(R.string.meetup_reminder_text)
            }

            NotificationHandler.Builder(applicationContext)
                .setContentText(
                    String.format(
                        notificationText,
                        reminder.person.name
                    )
                )
                .setContentTitle(application.getString(R.string.reminder_title))
                .setReminderId(reminder.reminder.id)
                .sendNotification()
    }


    companion object {
        fun enqueueWork(context: Context, work: Intent) {
            enqueueWork(context, NotificationService::class.java, NOTIFICATION_INTENT_ID, work)
        }
    }
}