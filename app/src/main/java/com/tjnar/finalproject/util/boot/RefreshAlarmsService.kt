package com.tjnar.finalproject.util.boot

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.JobIntentService
import com.tjnar.finalproject.data.Repository
import com.tjnar.finalproject.util.LOG_TAG
import com.tjnar.finalproject.util.REFRESH_ALARM_JOB_ID
import com.tjnar.finalproject.util.notifications.setSingleAlarm

class RefreshAlarmsService : JobIntentService() {
    override fun onHandleWork(intent: Intent) {
        Log.d(LOG_TAG, "Refresh Alarms Service")
        val repo = Repository.getInstance(application)
        val reminders = repo.getRemindersSync()

        reminders.forEach {
            setSingleAlarm(applicationContext, it.reminder.time.time)
        }
    }

    companion object {
        fun enqueueWork(context: Context, work: Intent) {
            enqueueWork(context, RefreshAlarmsService::class.java, REFRESH_ALARM_JOB_ID, work)
        }
    }
}