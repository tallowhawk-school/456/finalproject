package com.tjnar.finalproject.util.notifications

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.tjnar.finalproject.data.Repository
import com.tjnar.finalproject.util.LOG_TAG
import com.tjnar.finalproject.util.NOTIFICATION_INTENT_ID
import com.tjnar.finalproject.util.broadcast.NotifyBroadcastReceiver
import com.tjnar.finalproject.util.broadcast.OnBootBroadcastReceiver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


fun cancelAllAlarms(context: Context, app: Application) {
    val alarmManager =
        context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val notifyIntent = Intent(context, NotifyBroadcastReceiver::class.java)
    val pendingIntent = PendingIntent.getBroadcast(
        app,
        NOTIFICATION_INTENT_ID,
        notifyIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    // Cancel alarms
    try {
        alarmManager.cancel(pendingIntent)
    } catch (e: Exception) {
        Log.e(LOG_TAG, "Failed to cancel alarms")
    }
}

fun setAllAlarms(context: Context, app: Application) {
    Log.d(LOG_TAG,"Setting all alarms")

    val repo = Repository.getInstance(app)

    repo.reminders.value?.also {
        Log.d(LOG_TAG, it.size.toString())
    }
    repo.reminders.value?.forEach {
        if (!it.reminder.notificationSent) {
            setSingleAlarm(context, it.reminder.time.time)
        }
    }

    OnBootBroadcastReceiver.enableBootReceiver(context)
}

fun setSingleAlarm(context: Context, time: Long) {
    val notifyIntent = Intent(context, NotifyBroadcastReceiver::class.java)
    val pendingIntent = PendingIntent.getBroadcast(
        context,
        NOTIFICATION_INTENT_ID,
        notifyIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        alarmManager.setWindow(
            AlarmManager.RTC_WAKEUP,
            time,
            15000,
            pendingIntent
        )
    } else {
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent)
    }

}

fun refreshAlarms(context: Context, app: Application) {
    CoroutineScope(Dispatchers.IO).launch {
        val repo = Repository.getInstance(app)
        repo.reminders.observe(context as LifecycleOwner, Observer {
            cancelAllAlarms(context, app)
            setAllAlarms(context, app)
        })
    }
}
