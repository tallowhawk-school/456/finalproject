package com.tjnar.finalproject.util.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.tjnar.finalproject.util.notifications.NotificationService

class NotifyBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null && intent != null) {
            NotificationService.enqueueWork(context, intent)
        }
    }
}
