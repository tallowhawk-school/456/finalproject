package com.tjnar.finalproject.util.contacts

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.ContactsContract


fun getPhotoUri(id: String, context: Context?, thumbnail: Boolean = true): Uri? {
    try {
        context!!.contentResolver!!.query(
            ContactsContract.Data.CONTENT_URI,
            null,
            "${ContactsContract.Data.LOOKUP_KEY} = ? AND " +
                    "${ContactsContract.Data.MIMETYPE} = " +
                    "'${ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE}'",
            arrayOf(id),
            null
        ).use { cursor ->
            if (cursor != null && !cursor.moveToFirst()) {
                return null
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
        return null
    }
    val longId: Long = getIdFromLookup(id, context).let {
        if (it == null) {
            return null
        }
        return@let it
    }
    val person: Uri =
        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, longId)
    return Uri.withAppendedPath(person, if (thumbnail){
        ContactsContract.Contacts.Photo.CONTENT_DIRECTORY
    } else{
        ContactsContract.Contacts.Photo.DISPLAY_PHOTO
    })
}

fun getIdFromLookup(lookupKey: String, context: Context?): Long? {
    var id: Long? = null
    try {
        context!!.contentResolver!!.query(
            Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey),
            null, null, null, null
        ).use {  cursor ->
            if (cursor != null && cursor.moveToFirst()) {
                id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data._ID))
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
        return null
    }

    return id

}


fun getEmail(lookupKey: String, context: Context?): String {
    val selection = "${ContactsContract.Data.LOOKUP_KEY} = ?"
    val selectionArgs: Array<String> = arrayOf(lookupKey)
    var email = ""

    context?.contentResolver?.query(
        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
        null,
        selection,
        selectionArgs,
        null
    )
        .use { cursor ->
            if (cursor != null && cursor.moveToFirst()) {
                email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS))
            }

        }
    return email
}
