package com.tjnar.finalproject.util

import android.content.res.Resources
import android.os.Build
import java.util.Locale

fun locale(resources: Resources): Locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    resources.configuration.locales[0]
} else {
    resources.configuration.locale
}

const val CHANNEL_ID = "1"

const val NOTIFICATION_INTENT_ID = 11

const val REFRESH_ALARM_JOB_ID = 12

const val LOG_TAG = "final_project"

// Intents
const val PICK_CONTACT = 1

const val PERMISSIONS_REQUEST_READ_CONTACTS = 2

// Intent data passing
const val REMINDER_ID_INTENT_DATA = "reminderId"
