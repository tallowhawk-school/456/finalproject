package com.tjnar.finalproject.util.notifications

import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.tjnar.finalproject.MainActivity
import com.tjnar.finalproject.R
import com.tjnar.finalproject.util.REMINDER_ID_INTENT_DATA
import com.tjnar.finalproject.util.CHANNEL_ID
import java.util.concurrent.atomic.AtomicInteger

class NotificationHandler() {

    private constructor(builder: Builder) : this()

    class Builder(private val context: Context) {
        private val notification: NotificationCompat.Builder =
            NotificationCompat.Builder(context, CHANNEL_ID)

        private val intent = Intent(context, MainActivity::class.java)

        fun setContentTitle(title: String) = apply { notification.setContentTitle(title) }

        fun setContentText(text: String) = apply { notification.setContentText(text) }

        fun setReminderId(reminderId: Long) = apply {
            intent.putExtra(REMINDER_ID_INTENT_DATA, reminderId)
        }

        fun build(): Notification {
            return notification
                .setSmallIcon(R.drawable.ic_home_black_24dp)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(TaskStackBuilder.create(context).run {
                    addNextIntentWithParentStack(intent)
                    getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                })
                .build()
        }

        fun sendNotification() {
            with(NotificationManagerCompat.from(context)) {
                notify(getId(), this@Builder.build())
            }
        }
    }


    companion object {
        private val atomicInt = AtomicInteger(0)

        fun getId(): Int = atomicInt.incrementAndGet()

        fun createNotificationChannel(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = activity.getString(R.string.notification_channel_name)
                val descriptionText = activity.getString(R.string.notification_channel_description)
                val importance = NotificationManager.IMPORTANCE_HIGH
                val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                    description = descriptionText
                }
                // Register the channel with the system
                val notificationManager: NotificationManager =
                    activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }
    }

}
