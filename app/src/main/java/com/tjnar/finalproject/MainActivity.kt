package com.tjnar.finalproject

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tjnar.finalproject.data.ConnectionDatabase
import com.tjnar.finalproject.data.Repository
import com.tjnar.finalproject.data.models.ReminderType
import com.tjnar.finalproject.util.REMINDER_ID_INTENT_DATA
import com.tjnar.finalproject.util.notifications.NotificationHandler
import com.tjnar.finalproject.util.notifications.cancelAllAlarms
import com.tjnar.finalproject.util.notifications.setAllAlarms
import com.tjnar.finalproject.ui.reminders.RemindersViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    // https://stackoverflow.com/questions/866769/how-to-call-android-contacts-list

    override fun onStart() {
        super.onStart()
        NotificationHandler.createNotificationChannel(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_reminders, R.id.navigation_people
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val sharedPreferences =
            getSharedPreferences(getString(R.string.default_package_name), Context.MODE_PRIVATE)


        // On first run insert default db data.
        if (sharedPreferences.getBoolean("first_run", true)) {
            sharedPreferences.edit().putBoolean("first_run", false).apply()

            val reminderTypes = listOf(
                ReminderType(1, "Schedule Reminder"),
                ReminderType(2, "Meetup Reminder")
            )

            CoroutineScope(Dispatchers.IO).launch {
                val database = ConnectionDatabase.getDatabase(applicationContext)
                val reminderTypeDao = database.reminderTypeDAO()
                reminderTypeDao.insertReminderTypes(reminderTypes)
            }
        }

        val repo = Repository.getInstance(application)
        repo.reminders.observe(this, Observer {
            cancelAllAlarms(applicationContext, application)
            setAllAlarms(applicationContext, application)
        })

        val reminderId = intent.getLongExtra(REMINDER_ID_INTENT_DATA, -1L)
        if (reminderId != -1L) {
            val remindersViewModel =
                ViewModelProviders.of(this).get(RemindersViewModel::class.java)
            CoroutineScope(Dispatchers.IO).launch {
                val reminder = Repository.getInstance(application)
                    .getReminderById(reminderId)

                CoroutineScope(Dispatchers.Main).launch {
                    if (reminder != null) {
                        remindersViewModel.selectedReminder.value = reminder
                        navController.navigate(R.id.reminder)
                    }
                }
            }
        }
    }
}
